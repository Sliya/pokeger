import { UI } from './ui.js'
import { Supabase } from './supabase.js'

const filters = []

async function init() {
  search()
  document.getElementById('addFilter').onclick = addFilter
  document.getElementById('clearFilters').onclick = clearFilters
  document.getElementById('filter').onclick = search
  document.getElementById('login').onclick = login
  document.getElementById('logout').onclick = logout

  document.body.addEventListener('delete', (evt) => {
    filters.splice(filters.indexOf(evt.detail.filter), 1)
    evt.detail.remove()
  }, false)

  document.querySelectorAll('a.navbar-item').forEach(it => it.onclick = (e) => {
    e.preventDefault()
    nav(it.getAttribute('href'))
  })

  UI.addKebabAnimation()
  const user = await Supabase.loggedUser()
  if (user != null) {
    UI.userLoggedIn(user.email)
  }
}

async function login() {
  const { username, password } = UI.getCredentials()
  const loggedIn = await Supabase.signIn(username, password)
  if (loggedIn) {
    UI.userLoggedIn(username)
  }
  search()
}

function logout() {
  Supabase.logout()
  UI.userLoggedOut()
  search()
}

async function search() {
  const liste = await Supabase.loadData(filters)
  const isLoggedIn = await Supabase.loggedUser()
  UI.populate(liste, isLoggedIn)

  UI.populateStats(liste)
}

function clearFilters() {
  filters.splice(0, filters.length)
  UI.clearFilters()
}

function addFilter() {
  filters.push(UI.addFilter())
}

init()
