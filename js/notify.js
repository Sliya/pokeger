function notify(message, clazz) {
	const notif = document.createElement('div')
	notif.classList.add('notification', 'is-light', 'm-4', clazz)
	
	const deleteButton = document.createElement('button')
	deleteButton.classList.add('delete')
	notif.appendChild(deleteButton)
	deleteButton.onclick = () => notif.remove()

	const textNode = document.createTextNode(message)
	notif.appendChild(textNode)

	document.body.appendChild(notif)

	setTimeout(() => {
		notif.remove()
	}, 3000);
}

export const Notify = {
	error: (message) => {
		notify(message, 'is-danger')
	},

}
