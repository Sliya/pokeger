import { createClient } from 'https://cdn.jsdelivr.net/npm/@supabase/supabase-js/+esm'
import { Notify } from './notify.js'
const supabaseUrl = 'https://inrtcosmdakpcmyzpdqk.supabase.co'
const supabaseKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImlucnRjb3NtZGFrcGNteXpwZHFrIiwicm9sZSI6ImFub24iLCJpYXQiOjE2OTg2NjczNTgsImV4cCI6MjAxNDI0MzM1OH0.-qhrCSifO9H_zBI2x3-5vOkloZESkmttKt6NvJj3rpI'

const supabase = createClient(supabaseUrl, supabaseKey)

function filter(query, filter) {
  if (filter.operator == 'eq' && filter.value.includes('null')) {
    if (filter.value.includes('not')) {
      return query.not(filter.column, 'is', null)
    }
    return query.is(filter.column, null)
  }
  return query[filter.operator](filter.column, filter.value)
}

export const Supabase = {

  loadData: async (filters) => {
    let query = supabase.from('pokemons').select()
    filters.forEach(f => {
      query = filter(query, f)
    })
    const { data, error } = await query.order('id')
    if (error) {
      Notify.error('Une erreur a eu lieu, les filtres sont probablement incorrects')
      console.error(error)
      return []
    }
    return data
  },

  signIn: async (email, password) => {
    const { data, error } = await supabase.auth.signInWithPassword({ email, password })
    if (error) {
      Notify.error('Mauvais email ou mot de passe')
    }
    return data.user != null
  },

  loggedUser: async () => {
    const { data, error } = await supabase.auth.getSession()
    if (error) {
      console.error(error)
    }
    return data.session?.user
  },

  logout: async () => {
    const { error } = await supabase.auth.signOut()
    if (error) {
      console.error(error)
    }
  },

  update: async (id, column, value) => {
    const { error } = await supabase.from('pokemons').update({ [column]: value }).eq('id', id)
    if (error) {
      Notify.error('Impossible de modifier')
      console.error(error)
    }
  },
}
