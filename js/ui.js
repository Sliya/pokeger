import { Supabase } from './supabase.js'
const tags = document.getElementById('tags')
const table = document.querySelector('#tableListeContainer tbody')
const columnToFilterField = document.getElementById('columnToFilter')
const operatorField = document.getElementById('filterOperator')
const valueField = document.getElementById('filterValue')
const usernameField = document.getElementById('username')
const passwordField = document.getElementById('password')
const logInButton = document.getElementById('login')
const logOutButton = document.getElementById('logout')
const userField = document.getElementById('user')
const progressCaught = document.getElementById('progressCaught')
const progressLuxeball = document.getElementById('progressLuxeball')
const progressDO = document.getElementById('progressDO')
const progressPerfect = document.getElementById('progressPerfect')
const progressShiny = document.getElementById('progressShiny')
const modal = document.getElementById('modal')


function createCell(data, attribute, isLoggedIn) {
  const cell = document.createElement('td')
  const value = data[attribute]
  if (typeof value === 'boolean' || attribute == 'regionalForm') {
    cell.classList.add('has-text-centered')
    const checkbox = document.createElement('input')
    checkbox.type = 'checkbox'
    checkbox.checked = value
    checkbox.disabled = !isLoggedIn || (attribute == 'regionalForm' && data.regionalForm == null)
    cell.appendChild(checkbox)
    cell.onclick = () => Supabase.update(data.id, attribute, cell.querySelector('input').checked)
  } else {
    cell.innerText = value
  }
  return cell
}

function populateProgressBar(bar, value, nbPokemons) {
  bar.value = value
  bar.max = nbPokemons
  bar.title = `${value}/${nbPokemons} (${Math.round(value / nbPokemons * 100)}%)`
  bar.innerText = `${Math.round(value / nbPokemons * 100)}%`
  bar.dataset.full = value === nbPokemons
  bar.parentNode.querySelector("p.value").innerText = `${value} (${Math.round(value / nbPokemons * 100)}%)`
}

export const UI = {
  clearFilters: () => {
    tags.innerHTML = ''
  },

  addKebabAnimation: () => {
    // cf https://bulma.io/documentation/components/navbar/#navbar-menuhttps://bulma.io/documentation/components/navbar/#navbar-menu
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
    $navbarBurgers.forEach(el => {
      el.addEventListener('click', () => {
        const target = el.dataset.target;
        const $target = document.getElementById(target);
        el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  },

  populate: (data, isLoggedIn) => {
    table.innerHTML = ''
    data.forEach(pokemon => {
      const row = document.createElement('tr')
      row.appendChild(createCell(pokemon, 'id', isLoggedIn))
      row.appendChild(createCell(pokemon, 'name', isLoggedIn))
      row.appendChild(createCell(pokemon, 'generation', isLoggedIn))
      row.appendChild(createCell(pokemon, 'caught', isLoggedIn))
      row.appendChild(createCell(pokemon, 'caughtInLuxuryBall', isLoggedIn))
      row.appendChild(createCell(pokemon, 'caughtShiny', isLoggedIn))
      row.appendChild(createCell(pokemon, 'DOSliya', isLoggedIn))
      row.appendChild(createCell(pokemon, 'regionalForm', isLoggedIn))
      table.appendChild(row)
    })
  },

  populateStats: (liste) => {
    const nbCaught = liste.filter(it => it.caught).length
    populateProgressBar(progressCaught, nbCaught, liste.length)
    const nbInLuxeball = liste.filter(it => it.caughtInLuxuryBall).length
    populateProgressBar(progressLuxeball, nbInLuxeball, liste.length)
    const nbDO = liste.filter(it => it.DOSliya).length
    populateProgressBar(progressDO, nbDO, liste.length)
    const nbShiny = liste.filter(it => it.caughtShiny).length
    populateProgressBar(progressShiny, nbShiny, liste.length)
    const nbPerfect = liste.filter(it => !it.incorrect).length
    populateProgressBar(progressPerfect, nbPerfect, liste.length)
  },

  addFilter: () => {
    const column = columnToFilterField.value
    const operator = operatorField.value
    const value = valueField.value

    const filter = { column, operator, value }

    const tag = document.createElement('span')
    tag.classList.add('tag', 'is-primary', 'is-light')
    tag.innerText = columnToFilterField.selectedOptions[0].label
      + ' '
      + operatorField.selectedOptions[0].label
      + ' ' + value

    const closeButton = document.createElement('button')
    closeButton.classList.add('delete', 'is-small')
    closeButton.onclick = () => {
      tag.dispatchEvent(new CustomEvent('delete', {
        detail: {
          filter,
          remove: () => { tag.remove() },
        },
        bubbles: true,
      }))
    }
    tag.appendChild(closeButton)

    tags.appendChild(tag)
    return filter
  },

  getCredentials: () => {
    return { username: usernameField.value, password: passwordField.value }
  },

  userLoggedIn: (email) => {
    user.classList.remove('is-hidden')
    user.innerText = email
    usernameField.parentElement.classList.add('is-hidden')
    passwordField.parentElement.classList.add('is-hidden')
    logInButton.parentElement.classList.add('is-hidden')
    logOutButton.parentElement.classList.remove('is-hidden')
  },

  userLoggedOut: () => {
    user.classList.add('is-hidden')
    user.innerText = ''
    usernameField.parentElement.classList.remove('is-hidden')
    passwordField.parentElement.classList.remove('is-hidden')
    logInButton.parentElement.classList.remove('is-hidden')
    logOutButton.parentElement.classList.add('is-hidden')
  }
}
